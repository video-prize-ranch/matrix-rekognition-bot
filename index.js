const {
  MatrixClient,
  SimpleFsStorageProvider,
  AutojoinRoomsMixin,
} = require("matrix-bot-sdk");
const config = require("./config.json");
const fetch = require("node-fetch");

const { RekognitionClient, DetectModerationLabelsCommand } = require("@aws-sdk/client-rekognition");
const rekognition = new RekognitionClient({
  region: "us-east-1",
  credentials: {
    accessKeyId: config.ACCESS_KEY_ID,
    secretAccessKey: config.SECRET_ACCESS_KEY
  }
});

const storage = new SimpleFsStorageProvider("bot.json");
const client = new MatrixClient(config.HOMESERVER_URL, config.ACCESS_TOKEN, storage);
AutojoinRoomsMixin.setupOnClient(client);

client.on("room.message", handleCommand);

client.start().then(() => console.log("Client started!"));

async function handleCommand(roomId, event) {
  if (!event["content"]) return;

  if (event["content"]["msgtype"] !== "m.image") return;

  if (event["sender"] === await client.getUserId()) return;

  if (!["image/jpeg", "image/png"].includes(event["content"]["info"]["mimetype"])) return;

  const inputImage = client.mxcToHttp(event["content"]["url"])

  const res = await fetch(inputImage);
  const inputImageBuffer = await res.buffer();

  const command = new DetectModerationLabelsCommand({
    Image: {
      Bytes: inputImageBuffer
    }
  });
  const response = await rekognition.send(command);

  if (response.ModerationLabels.length === 0) return;

  let responseStrings = [];
  let categories = [];
  response.ModerationLabels.forEach(label => {
    categories.push(label.ParentName);
    categories.push(label.Name);
    responseStrings.push(`${label.Confidence.toString().slice(0, 2)}% ${label.ParentName}/${label.Name}`);
  })

  if (config.DISALLOW.some(r => categories.includes(r))) {
    client.redactEvent(roomId, event["event_id"], `Rekognition response: ${responseStrings.toString().replace(/,/g, ", ")}`)

    client.sendMessage(roomId, {
      "msgtype": "m.text",
      "body": `Rekognition response: ${responseStrings.toString().replace(/,/g, ", ")}`,
      "formatted_body": `<b>Rekognition response</b>: ${responseStrings.toString().replace(/,/g, ", ")}`,
    });
  }
}
# matrix-rekognition-bot
Use AWS Rekognition to detect unsafe content and redact it.

## Usage
You will need an AWS account. It is recommended to make a new user in IAM (attach policy AmazonRekognitionFullAccess) but for testing you may use your root account. Your first 5,000 images in the 12 month free tier are free, then $0.001/image for the next 1 million images.

### Example configuration
For categories you can DISALLOW, see ![https://docs.aws.amazon.com/rekognition/latest/dg/moderation.html#moderation-api](https://docs.aws.amazon.com/rekognition/latest/dg/moderation.html#moderation-api). You can use both top-level and second-level categories.

```json
{
  "ACCESS_TOKEN": "https://t2bot.io/docs/access_tokens",
  "HOMESERVER_URL": "https://matrix.org",
  "ACCESS_KEY_ID": "",
  "SECRET_ACCESS_KEY": "",
  "DISALLOW": ["Explicit Nudity", "Suggestive", "Weapon Violence", "Visually Disturbing", "Rude Gestures", "Drinking", "Tobacco", "Alcohol", "Hate Symbols"]
}
```